class Main { 
	attr_Int1 : Int <- 5;
	main() : Int {0};
};

class Child1 inherits Main { 
	attr_Int2 : Int <- 6;
	main() : Int {0};
};

class Child2 inherits Child1 { 
	attr_Int3 : Int <- 7;
	main() : Int {0};
};

class Child3 inherits Child2 { 
	attr_Int4 : Int <- 8;
	main() : Int {0};
};