class Main { 
	main() : Int { 0 }; 
	moo() : String { "moo" };
};

class Cow inherits Main {
	moo() : String { "cow!" };
};
