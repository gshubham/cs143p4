class Main { 
	attr_Bool1 : Bool;
	main() : Int {0};
};

class Child1 inherits Main { 
	attr_Bool2 : Bool;
	main() : Int {0};
};

class Child2 inherits Child1 { 
	attr_Bool3 : Bool;
	main() : Int {0};
};

class Child3 inherits Child2 { 
	attr_Bool4 : Bool;
	main() : Int {0};
};