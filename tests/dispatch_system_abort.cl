(* Simple check to see whether an abort call (system) works *)

class Main {
  main():Int {{ abort(); 0; }};
};