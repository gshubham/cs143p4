class Main { 
	attr_Int1 : Int <- 5;
	main() : Int {0};
};

class Child1 inherits Main { 
	attr_Int2 : Int <- 6;
	main() : Int {0};
};

class Child2 inherits Child1 { 
	attr_Int3 : Int <- 7;
	main() : Int {0};
};

class Child3 inherits Child2 { 
	attr_Int4 : Int <- 8;
	attr_Int5 : Int <- 5+(6+(7+(8+(9+(4+(5+(457+87)+457)+8745)+451)+5245+745)+421)+45);
	attr_Int6 : Int <- 9;
	attr_Int7 : Int <- 9;
	attr_Int8 : Int <- 9;
	attr_Int9 : Int <- 9;
	attr_Int10 : Int <- 9;
	attr_Int11 : Int <- 9;
	attr_Int12 : Int <- 9;
	attr_Int13 : Int <- 9;
	attr_Int14 : Int <- 9;
	attr_Int15 : Int <- 9;
	attr_Int16 : Int <- 9;
	attr_Int17 : Int <- 9;
	main() : Int {0};
};