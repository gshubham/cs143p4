class Main inherits IO {
  x : Int;
  temp : Int;
  f(y : Int) : SELF_TYPE { {
    out_int(y);
    out_string("\n");
    out_int(x + y);
    out_string("\n");
    self;
  } };
  g(z : Int) : Int {
    z + x
  };
  main() : Object {
    {
      out_string("temp: ");
      out_int(temp);
      out_string("\n");
      temp <- g(1);
      out_string("temp: ");
      out_int(temp);
      out_string("\n");
    }
  };
};
