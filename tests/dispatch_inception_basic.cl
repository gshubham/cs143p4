class Main inherits IO {
  x : Int;
  temp : Int;
  f(y : Int) : SELF_TYPE { {
    out_int(y);
    out_string("\n");
    out_int(x + y);
    out_string("\n");
    self;
  } };
  g(z : Int) : Int { {
    out_string("g - x: ");
    out_int(x);
    out_string("\n");
    x <- x + 1;
    out_string("g - (inc) x: ");
    out_int(x);
    out_string("\n");
    out_string("g - z: ");
    out_int(z);
    out_string("\n");
    z + x;
  } };
  main() : Object {
    {
      out_string("temp: ");
      out_int(temp);
      temp <- g(1);
      out_string("temp: ");
      out_int(temp);
      out_string("\n");
    }
  };
};
