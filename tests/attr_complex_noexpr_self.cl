class A inherits IO {
  b : Bool <- true;
  y : Int <- z + 3;
  z : Int <- z - 5;

  print_attr() : Object { {
   out_string("b: ");
   out_string(if b then "true" else "false" fi);
   out_string("\ny: ");
   out_int(y);
   out_string("\nz: ");
   out_int(z);
  } };
};

class Main {
  a : A <- new A;
  main() : Object {
    a.print_attr()
  };
};