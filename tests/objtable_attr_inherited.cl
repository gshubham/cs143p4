class Main { main() : Int {0}; };
class C1 inherits Main { attr1 : Int <- 1; };
class C2 inherits C1 { attr2 : Int <- 2; };
class C3 inherits C2 { attr3 : Int <- 3; };
class C4 inherits C3 { attr4 : Int <- 4; };
class C5 inherits C4 { attr5 : Int <- 5; };
