class Main { 
	attr_Int1 : Int;
	main() : Int {0};
};

class Child1 inherits Main { 
	attr_Int2 : Int;
	main() : Int {0};
};

class Child2 inherits Child1 { 
	attr_Int3 : Int;
	main() : Int {0};
};

class Child3 inherits Child2 { 
	attr_Int4 : Int;
	main() : Int {0};
};