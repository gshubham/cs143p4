class Main {
	main () : Int {0};
	test_case () : Object {
		case 5 of
			a : Int => 1;
			b : Bool => 2;
			c : Object => 3;
			d : A => 4;
		esac
	};	
};

class A {};
class B {};