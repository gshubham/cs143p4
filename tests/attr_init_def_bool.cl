class Main inherits IO {
  booltest : Bool;

  main():Object {{
    if booltest then
      out_string("true")
    else
      out_string("false")
    fi;
  }};
};