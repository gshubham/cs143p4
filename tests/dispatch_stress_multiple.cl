class Main inherits IO {
  x : Int;
  f(y : Int) : SELF_TYPE { {
    out_int(x + y);
    out_string("\n");
    self;
  } };
  g(z : Int) : Int { {
    out_string("g - x: ");
    out_int(x);
    out_string("\n");
    x <- x + 1;
    out_string("g - z: ");
    out_int(z);
    out_string("\n");
    z + x;
  } };
  main() : Object {
    f(g(1)).f(g(g(5))).f(g(g(g(10))))
  };
};
