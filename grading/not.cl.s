# start of generated code
	.data
	.align	2
	.globl	class_nameTab
	.globl	Main_protObj
	.globl	Int_protObj
	.globl	String_protObj
	.globl	bool_const0
	.globl	bool_const1
	.globl	_int_tag
	.globl	_bool_tag
	.globl	_string_tag
_int_tag:
	.word	2
_bool_tag:
	.word	3
_string_tag:
	.word	4
	.globl	_MemMgr_INITIALIZER
_MemMgr_INITIALIZER:
	.word	_NoGC_Init
	.globl	_MemMgr_COLLECTOR
_MemMgr_COLLECTOR:
	.word	_NoGC_Collect
	.globl	_MemMgr_TEST
_MemMgr_TEST:
	.word	0
	.word	-1
str_const11:
	.word	4
	.word	5
	.word	String_dispTab
	.word	int_const0
	.byte	0	
	.align	2
	.word	-1
str_const10:
	.word	4
	.word	6
	.word	String_dispTab
	.word	int_const3
	.ascii	"Main"
	.byte	0	
	.align	2
	.word	-1
str_const9:
	.word	4
	.word	6
	.word	String_dispTab
	.word	int_const4
	.ascii	"String"
	.byte	0	
	.align	2
	.word	-1
str_const8:
	.word	4
	.word	6
	.word	String_dispTab
	.word	int_const3
	.ascii	"Bool"
	.byte	0	
	.align	2
	.word	-1
str_const7:
	.word	4
	.word	5
	.word	String_dispTab
	.word	int_const5
	.ascii	"Int"
	.byte	0	
	.align	2
	.word	-1
str_const6:
	.word	4
	.word	5
	.word	String_dispTab
	.word	int_const6
	.ascii	"IO"
	.byte	0	
	.align	2
	.word	-1
str_const5:
	.word	4
	.word	6
	.word	String_dispTab
	.word	int_const4
	.ascii	"Object"
	.byte	0	
	.align	2
	.word	-1
str_const4:
	.word	4
	.word	7
	.word	String_dispTab
	.word	int_const7
	.ascii	"_prim_slot"
	.byte	0	
	.align	2
	.word	-1
str_const3:
	.word	4
	.word	7
	.word	String_dispTab
	.word	int_const8
	.ascii	"SELF_TYPE"
	.byte	0	
	.align	2
	.word	-1
str_const2:
	.word	4
	.word	7
	.word	String_dispTab
	.word	int_const8
	.ascii	"_no_class"
	.byte	0	
	.align	2
	.word	-1
str_const1:
	.word	4
	.word	8
	.word	String_dispTab
	.word	int_const9
	.ascii	"<basic class>"
	.byte	0	
	.align	2
	.word	-1
str_const0:
	.word	4
	.word	7
	.word	String_dispTab
	.word	int_const10
	.ascii	"./not.cl"
	.byte	0	
	.align	2
	.word	-1
int_const10:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	8
	.word	-1
int_const9:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	13
	.word	-1
int_const8:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	9
	.word	-1
int_const7:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	10
	.word	-1
int_const6:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	2
	.word	-1
int_const5:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	3
	.word	-1
int_const4:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	6
	.word	-1
int_const3:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	4
	.word	-1
int_const2:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	1
	.word	-1
int_const1:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	100
	.word	-1
int_const0:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	0
	.word	-1
bool_const0:
	.word	3
	.word	4
	.word	Bool_dispTab
	.word	0
	.word	-1
bool_const1:
	.word	3
	.word	4
	.word	Bool_dispTab
	.word	1
class_nameTab:
	.word	str_const5
	.word	str_const6
	.word	str_const7
	.word	str_const8
	.word	str_const9
	.word	str_const10
class_objTab:
	.word	Object_protObj
	.word	Object_init
	.word	IO_protObj
	.word	IO_init
	.word	Int_protObj
	.word	Int_init
	.word	Bool_protObj
	.word	Bool_init
	.word	String_protObj
	.word	String_init
	.word	Main_protObj
	.word	Main_init
Object_dispTab:
	.word	Object.abort
	.word	Object.type_name
	.word	Object.copy
IO_dispTab:
	.word	Object.abort
	.word	Object.type_name
	.word	Object.copy
	.word	IO.out_string
	.word	IO.out_int
	.word	IO.in_string
	.word	IO.in_int
Int_dispTab:
	.word	Object.abort
	.word	Object.type_name
	.word	Object.copy
Bool_dispTab:
	.word	Object.abort
	.word	Object.type_name
	.word	Object.copy
String_dispTab:
	.word	Object.abort
	.word	Object.type_name
	.word	Object.copy
	.word	String.length
	.word	String.concat
	.word	String.substr
Main_dispTab:
	.word	Object.abort
	.word	Object.type_name
	.word	Object.copy
	.word	Main.main
	.word	-1
Object_protObj:
	.word	0
	.word	3
	.word	Object_dispTab
	.word	-1
IO_protObj:
	.word	1
	.word	3
	.word	IO_dispTab
	.word	-1
Int_protObj:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	0
	.word	-1
Bool_protObj:
	.word	3
	.word	4
	.word	Bool_dispTab
	.word	0
	.word	-1
String_protObj:
	.word	4
	.word	5
	.word	String_dispTab
	.word	int_const0
	.word	0
	.word	-1
Main_protObj:
	.word	5
	.word	3
	.word	Main_dispTab
	.globl	heap_start
heap_start:
	.word	0
	.text
	.globl	Main_init
	.globl	Int_init
	.globl	String_init
	.globl	Bool_init
	.globl	Main.main
Object_init:
	addiu	$sp $sp -12
	sw	$fp 12($sp)
	sw	$s0 8($sp)
	sw	$ra 4($sp)
	addiu	$fp $sp 12
	move	$s0 $a0
	move	$a0 $s0
	lw	$fp 12($sp)
	lw	$s0 8($sp)
	lw	$ra 4($sp)
	addiu	$sp $sp 12
	jr	$ra	
IO_init:
	addiu	$sp $sp -12
	sw	$fp 12($sp)
	sw	$s0 8($sp)
	sw	$ra 4($sp)
	addiu	$fp $sp 12
	move	$s0 $a0
	jal	Object_init
	move	$a0 $s0
	lw	$fp 12($sp)
	lw	$s0 8($sp)
	lw	$ra 4($sp)
	addiu	$sp $sp 12
	jr	$ra	
Int_init:
	addiu	$sp $sp -12
	sw	$fp 12($sp)
	sw	$s0 8($sp)
	sw	$ra 4($sp)
	addiu	$fp $sp 12
	move	$s0 $a0
	jal	Object_init
	move	$a0 $s0
	lw	$fp 12($sp)
	lw	$s0 8($sp)
	lw	$ra 4($sp)
	addiu	$sp $sp 12
	jr	$ra	
Bool_init:
	addiu	$sp $sp -12
	sw	$fp 12($sp)
	sw	$s0 8($sp)
	sw	$ra 4($sp)
	addiu	$fp $sp 12
	move	$s0 $a0
	jal	Object_init
	move	$a0 $s0
	lw	$fp 12($sp)
	lw	$s0 8($sp)
	lw	$ra 4($sp)
	addiu	$sp $sp 12
	jr	$ra	
String_init:
	addiu	$sp $sp -12
	sw	$fp 12($sp)
	sw	$s0 8($sp)
	sw	$ra 4($sp)
	addiu	$fp $sp 12
	move	$s0 $a0
	jal	Object_init
	move	$a0 $s0
	lw	$fp 12($sp)
	lw	$s0 8($sp)
	lw	$ra 4($sp)
	addiu	$sp $sp 12
	jr	$ra	
Main_init:
	addiu	$sp $sp -12
	sw	$fp 12($sp)
	sw	$s0 8($sp)
	sw	$ra 4($sp)
	addiu	$fp $sp 12
	move	$s0 $a0
	jal	Object_init
	move	$a0 $s0
	lw	$fp 12($sp)
	lw	$s0 8($sp)
	lw	$ra 4($sp)
	addiu	$sp $sp 12
	jr	$ra	
Main.main:
	addiu	$sp $sp -68
	sw	$fp 68($sp)
	sw	$s0 64($sp)
	sw	$ra 60($sp)
	addiu	$fp $sp 68
	move	$s0 $a0
	la	$a0 int_const0
	la	$a0 int_const0
	sw	$a0 -12($fp)
	la	$a0 bool_const0
	sw	$a0 -16($fp)
label0:
	lw	$a0 -12($fp)
	sw	$a0 -20($fp)
	la	$a0 int_const1
	lw	$t1 -20($fp)
	lw	$t1 12($t1)
	lw	$a0 12($a0)
	slt	$a0 $t1 $a0
	sw	$a0 -20($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -20($fp)
	sw	$t1 12($a0)
	lw	$a0 12($a0)
	beq	$a0 $zero label1
	lw	$a0 -16($fp)
	sw	$a0 -20($fp)
	lw	$a0 -16($fp)
	lw	$a0 12($a0)
	beq	$a0 $zero label2
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label3
label2:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label3:
	lw	$a0 12($a0)
	beq	$a0 $zero label4
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label5
label4:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label5:
	lw	$a0 12($a0)
	beq	$a0 $zero label6
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label7
label6:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label7:
	lw	$a0 12($a0)
	beq	$a0 $zero label8
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label9
label8:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label9:
	lw	$a0 12($a0)
	beq	$a0 $zero label10
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label11
label10:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label11:
	lw	$a0 12($a0)
	beq	$a0 $zero label12
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label13
label12:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label13:
	lw	$a0 12($a0)
	beq	$a0 $zero label14
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label15
label14:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label15:
	lw	$a0 12($a0)
	beq	$a0 $zero label16
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label17
label16:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label17:
	lw	$a0 12($a0)
	beq	$a0 $zero label18
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label19
label18:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label19:
	lw	$a0 12($a0)
	beq	$a0 $zero label20
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label21
label20:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label21:
	lw	$a0 12($a0)
	beq	$a0 $zero label22
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label23
label22:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label23:
	lw	$t1 -20($fp)
	move	$t2 $a0
	la	$a0 bool_const1
	beq	$t1 $t2 label24
	la	$a1 bool_const0
	jal	equality_test
label24:
	lw	$a0 -16($fp)
	sw	$a0 -20($fp)
	lw	$a0 -16($fp)
	lw	$a0 12($a0)
	beq	$a0 $zero label25
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label26
label25:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label26:
	lw	$a0 12($a0)
	beq	$a0 $zero label27
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label28
label27:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label28:
	lw	$a0 12($a0)
	beq	$a0 $zero label29
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label30
label29:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label30:
	lw	$a0 12($a0)
	beq	$a0 $zero label31
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label32
label31:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label32:
	lw	$a0 12($a0)
	beq	$a0 $zero label33
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label34
label33:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label34:
	lw	$a0 12($a0)
	beq	$a0 $zero label35
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label36
label35:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label36:
	lw	$a0 12($a0)
	beq	$a0 $zero label37
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label38
label37:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label38:
	lw	$a0 12($a0)
	beq	$a0 $zero label39
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label40
label39:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label40:
	lw	$a0 12($a0)
	beq	$a0 $zero label41
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label42
label41:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label42:
	lw	$a0 12($a0)
	beq	$a0 $zero label43
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label44
label43:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label44:
	lw	$a0 12($a0)
	beq	$a0 $zero label45
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label46
label45:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label46:
	lw	$t1 -20($fp)
	move	$t2 $a0
	la	$a0 bool_const1
	beq	$t1 $t2 label47
	la	$a1 bool_const0
	jal	equality_test
label47:
	lw	$a0 -16($fp)
	sw	$a0 -20($fp)
	lw	$a0 -16($fp)
	lw	$a0 12($a0)
	beq	$a0 $zero label48
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label49
label48:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label49:
	lw	$a0 12($a0)
	beq	$a0 $zero label50
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label51
label50:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label51:
	lw	$a0 12($a0)
	beq	$a0 $zero label52
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label53
label52:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label53:
	lw	$a0 12($a0)
	beq	$a0 $zero label54
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label55
label54:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label55:
	lw	$a0 12($a0)
	beq	$a0 $zero label56
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label57
label56:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label57:
	lw	$a0 12($a0)
	beq	$a0 $zero label58
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label59
label58:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label59:
	lw	$a0 12($a0)
	beq	$a0 $zero label60
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label61
label60:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label61:
	lw	$a0 12($a0)
	beq	$a0 $zero label62
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label63
label62:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label63:
	lw	$a0 12($a0)
	beq	$a0 $zero label64
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label65
label64:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label65:
	lw	$a0 12($a0)
	beq	$a0 $zero label66
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label67
label66:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label67:
	lw	$a0 12($a0)
	beq	$a0 $zero label68
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label69
label68:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label69:
	lw	$t1 -20($fp)
	move	$t2 $a0
	la	$a0 bool_const1
	beq	$t1 $t2 label70
	la	$a1 bool_const0
	jal	equality_test
label70:
	lw	$a0 -16($fp)
	sw	$a0 -20($fp)
	lw	$a0 -16($fp)
	lw	$a0 12($a0)
	beq	$a0 $zero label71
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label72
label71:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label72:
	lw	$a0 12($a0)
	beq	$a0 $zero label73
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label74
label73:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label74:
	lw	$a0 12($a0)
	beq	$a0 $zero label75
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label76
label75:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label76:
	lw	$a0 12($a0)
	beq	$a0 $zero label77
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label78
label77:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label78:
	lw	$a0 12($a0)
	beq	$a0 $zero label79
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label80
label79:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label80:
	lw	$a0 12($a0)
	beq	$a0 $zero label81
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label82
label81:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label82:
	lw	$a0 12($a0)
	beq	$a0 $zero label83
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label84
label83:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label84:
	lw	$a0 12($a0)
	beq	$a0 $zero label85
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label86
label85:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label86:
	lw	$a0 12($a0)
	beq	$a0 $zero label87
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label88
label87:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label88:
	lw	$a0 12($a0)
	beq	$a0 $zero label89
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label90
label89:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label90:
	lw	$a0 12($a0)
	beq	$a0 $zero label91
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label92
label91:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label92:
	lw	$t1 -20($fp)
	move	$t2 $a0
	la	$a0 bool_const1
	beq	$t1 $t2 label93
	la	$a1 bool_const0
	jal	equality_test
label93:
	lw	$a0 -16($fp)
	sw	$a0 -20($fp)
	lw	$a0 -16($fp)
	lw	$a0 12($a0)
	beq	$a0 $zero label94
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label95
label94:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label95:
	lw	$a0 12($a0)
	beq	$a0 $zero label96
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label97
label96:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label97:
	lw	$a0 12($a0)
	beq	$a0 $zero label98
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label99
label98:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label99:
	lw	$a0 12($a0)
	beq	$a0 $zero label100
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label101
label100:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label101:
	lw	$a0 12($a0)
	beq	$a0 $zero label102
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label103
label102:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label103:
	lw	$a0 12($a0)
	beq	$a0 $zero label104
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label105
label104:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label105:
	lw	$a0 12($a0)
	beq	$a0 $zero label106
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label107
label106:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label107:
	lw	$a0 12($a0)
	beq	$a0 $zero label108
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label109
label108:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label109:
	lw	$a0 12($a0)
	beq	$a0 $zero label110
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label111
label110:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label111:
	lw	$a0 12($a0)
	beq	$a0 $zero label112
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label113
label112:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label113:
	lw	$a0 12($a0)
	beq	$a0 $zero label114
	move	$a0 $zero
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
	b	label115
label114:
	li	$a0 1
	sw	$a0 -24($fp)
	la	$a0 Bool_protObj
	jal	Object.copy
	lw	$t1 -24($fp)
	sw	$t1 12($a0)
label115:
	lw	$t1 -20($fp)
	move	$t2 $a0
	la	$a0 bool_const1
	beq	$t1 $t2 label116
	la	$a1 bool_const0
	jal	equality_test
label116:
	lw	$a0 -12($fp)
	sw	$a0 -20($fp)
	la	$a0 int_const2
	lw	$t1 -20($fp)
	lw	$t1 12($t1)
	lw	$a0 12($a0)
	add	$a0 $t1 $a0
	sw	$a0 -20($fp)
	la	$a0 Int_protObj
	jal	Object.copy
	lw	$t1 -20($fp)
	sw	$t1 12($a0)
	sw	$a0 -12($fp)
	b	label0
label1:
	move	$a0 $zero
	lw	$a0 -16($fp)
	lw	$a0 12($a0)
	beq	$a0 $zero label117
	move	$a0 $s0
	bne	$a0 $zero label118
	la	$a0 str_const0
	li	$t1 18
	jal	_dispatch_abort
label118:
	lw	$t1 8($a0)
	lw	$t1 0($t1)
	jalr		$t1
	b	label119
label117:
	la	$a0 int_const0
label119:
	lw	$fp 68($sp)
	lw	$s0 64($sp)
	lw	$ra 60($sp)
	addiu	$sp $sp 68
	jr	$ra	

# end of generated code
