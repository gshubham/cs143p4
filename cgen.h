#include <assert.h>
#include <stdio.h>
#include "emit.h"
#include "cool-tree.h"
#include "symtab.h"
#include <map>
#include <vector>
#include <stack>

enum Basicness     {Basic, NotBasic};
enum Memory        {Parameter, Local, Attribute};
#define TRUE 1
#define FALSE 0

#define AR_METADATA 3

class CgenClassTable;
typedef CgenClassTable *CgenClassTableP;

class CgenNode;
typedef CgenNode *CgenNodeP;

struct meth_disp {
  Symbol class_name;
  Symbol meth_name;
  Feature method;
};

struct location {
  Memory type;
  int offset;
};

class CgenNode : public class__class {
private: 
   CgenNodeP parentnd;                        // Parent of class
   List<CgenNode> *children;                  // Children of class
   Basicness basic_status;                    // `Basic' if class is basic
                                              // `NotBasic' otherwise

public:
   CgenNode(Class_ c,
            Basicness bstatus,
            CgenClassTableP class_table);

   void add_child(CgenNodeP child);
   List<CgenNode> *get_children() { return children; }
   void set_parentnd(CgenNodeP p);
   CgenNodeP get_parentnd() { return parentnd; }
   int basic() { return (basic_status == Basic); }
};


class CgenClassTable : public SymbolTable<Symbol,CgenNode> {
private:
   List<CgenNode> *nds;
   ostream& str;
   int stringclasstag;
   int intclasstag;
   int boolclasstag;
   SymbolTable<Symbol, location> *env;
   std::stack <int> temporaries;

// Used to handle current class
   CgenNodeP curr_class;

// Used to assign class tags
   int class_counter;
   int temp_number;
   int max_temps;

// The following methods emit code for
// constants and global declarations.

   void code_global_data();
   void code_global_text();
   void code_bools(int);
   void code_select_gc();
   void code_constants();

// The following methods emit code for
// basic object prototype

  void code_prototype_objects();
  void code_class_nametab();
  void code_class_objtab();
  void code_dispatch_table();

// The following methods emit code 
  void code_object_initializers();
  void code_class_methods();
  void code_methods(CgenNodeP);
  void attr_ref(Feature);

// These are helpers to initialize class
  int init_get_nt(std::vector<Feature>, int);
// The following handle setting up the methods
  void code_method(CgenNodeP, Feature);
  void emit_method_init(int);
  void emit_method_ret(int, int);

// The following are helper utilities
// to set up helper data structures
  void assign_class_tags(CgenNodeP curr);
  void assign_basic_tags();
  CgenNodeP tag_to_class(int tag);
  int find_redef(Symbol method_name, std::vector<meth_disp>& disp_tab);
  void build_dispatch_tbl(CgenNodeP curr);
  std::vector<meth_disp> build_dispatch_tbl(CgenNodeP, std::vector<meth_disp>);
  void build_attr_tbl(CgenNodeP);
  std::vector<Feature> build_attr_tbl(CgenNodeP, std::vector<Feature>);
  void build_object_initializers(CgenNodeP curr, int counter);
  void add_attr(CgenNodeP);
  

// The following creates an inheritance graph from
// a list of classes.  The graph is implemented as
// a tree of `CgenNode', and class names are placed
// in the base class symbol table.

   void install_basic_classes();
   void install_class(CgenNodeP nd);
   void install_classes(Classes cs);
   void build_inheritance_tree();
   void set_relations(CgenNodeP nd);

// We need a set of functions to interface with the environment.
// 1. Ask for a new persistent store. 
// 2. Keep track of temporaries storied
// 3. Be able to "give up" temporaries stored
// 4. 
public:
   // Managing class tags
   // TODO: Pretty sure this can all be private w/o problems
   std::map<CgenNodeP, int> class_tags;
   std::map<CgenNodeP, int> max_class_tags;
   std::map<CgenNodeP, std::vector<meth_disp> > dispatch_tbl;
   std::map<CgenNodeP, std::vector<Feature> > attr_tbl;
   int label_counter;

   // Interface to manage temporaries for NT
   void enter_method(int number_temps);
   void exit_method();
   void enter_expr();
   void exit_expr();
   void new_scope() { env->enterscope(); }
   void end_scope() { env->exitscope(); }
   int get_temp();
   location* get_env(Symbol sym);
   void add_env(Symbol sym, location * new_loc);
   void max_class_tag(CgenNodeP curr, int& max);
   void assign_max_class_tags(CgenNodeP curr);
   // Used to determine const labels
   int get_int_class_tag() { return intclasstag; }
   int get_string_class_tag() { return stringclasstag; }
   int get_bool_class_tag() { return boolclasstag; }

   ostream& get_stream() {
    return str;
   }

   int get_label_counter() {
    int return_value = label_counter;
    label_counter++;
    return return_value;
   }

   CgenClassTable(Classes, ostream& str);
   void code();
   CgenNodeP root();

   // Functions related to current class
   char* get_filename() { return curr_class->filename->get_string(); }
   CgenNodeP get_curr_class() { return curr_class; }
   CgenNodeP find_class (Symbol);
};


class BoolConst 
{
 private: 
  int val;
 public:
  BoolConst(int);
  void code_def(ostream&, int boolclasstag);
  void code_ref(ostream&) const;
};

