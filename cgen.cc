//**************************************************************
//
// Code generator SKELETON
//
// Read the comments carefully. Make sure to
//    initialize the base class tags in
//       `CgenClassTable::CgenClassTable'
//
//    Add the label for the dispatch tables to
//       `IntEntry::code_def'
//       `StringEntry::code_def'
//       `BoolConst::code_def'

//
//    Add code to emit everyting else that is needed
//       in `CgenClassTable::code'
//
//
// The files as provided will produce code to begin the code
// segments, declare globals, and emit constants.  You must
// fill in the rest.
//
//**************************************************************

#include "cgen.h"
#include "cgen_gc.h"
#include <queue>

#define AR_METADATA 3

extern void emit_string_constant(ostream& str, char *s);
extern int cgen_debug;


//
// Three symbols from the semantic analyzer (semant.cc) are used.
// If e : No_type, then no code is generated for e.
// Special code is generated for new SELF_TYPE.
// The name "self" also generates code different from other references.
//
//////////////////////////////////////////////////////////////////////
//
// Symbols
//
// For convenience, a large number of symbols are predefined here.
// These symbols include the primitive type and method names, as well
// as fixed names used by the runtime system.
//
//////////////////////////////////////////////////////////////////////
Symbol 
       arg,
       arg2,
       Bool,
       concat,
       cool_abort,
       copy,
       Int,
       in_int,
       in_string,
       IO,
       length,
       Main,
       main_meth,
       No_class,
       No_type,
       Object,
       out_int,
       out_string,
       prim_slot,
       self,
       SELF_TYPE,
       Str,
       str_field,
       substr,
       type_name,
       val;
//
// Initializing the predefined symbols.
//
static void initialize_constants(void)
{
  arg         = idtable.add_string("arg");
  arg2        = idtable.add_string("arg2");
  Bool        = idtable.add_string("Bool");
  concat      = idtable.add_string("concat");
  cool_abort  = idtable.add_string("abort");
  copy        = idtable.add_string("copy");
  Int         = idtable.add_string("Int");
  in_int      = idtable.add_string("in_int");
  in_string   = idtable.add_string("in_string");
  IO          = idtable.add_string("IO");
  length      = idtable.add_string("length");
  Main        = idtable.add_string("Main");
  main_meth   = idtable.add_string("main");
//   _no_class is a symbol that can't be the name of any 
//   user-defined class.
  No_class    = idtable.add_string("_no_class");
  No_type     = idtable.add_string("_no_type");
  Object      = idtable.add_string("Object");
  out_int     = idtable.add_string("out_int");
  out_string  = idtable.add_string("out_string");
  prim_slot   = idtable.add_string("_prim_slot");
  self        = idtable.add_string("self");
  SELF_TYPE   = idtable.add_string("SELF_TYPE");
  Str         = idtable.add_string("String");
  str_field   = idtable.add_string("_str_field");
  substr      = idtable.add_string("substr");
  type_name   = idtable.add_string("type_name");
  val         = idtable.add_string("_val");
}

static char *gc_init_names[] =
  { "_NoGC_Init", "_GenGC_Init", "_ScnGC_Init" };
static char *gc_collect_names[] =
  { "_NoGC_Collect", "_GenGC_Collect", "_ScnGC_Collect" };


//  BoolConst is a class that implements code generation for operations
//  on the two booleans, which are given global names here.
BoolConst falsebool(FALSE);
BoolConst truebool(TRUE);

//*********************************************************
//
// Define method for code generation
//
// This is the method called by the compiler driver
// `cgtest.cc'. cgen takes an `ostream' to which the assembly will be
// emmitted, and it passes this and the class list of the
// code generator tree to the constructor for `CgenClassTable'.
// That constructor performs all of the work of the code
// generator.
//
//*********************************************************

void program_class::cgen(ostream &os) 
{
  // spim wants comments to start with '#'
  os << "# start of generated code\n";

  initialize_constants();
  CgenClassTable *codegen_classtable = new CgenClassTable(classes,os);

  os << "\n# end of generated code\n";
}


//////////////////////////////////////////////////////////////////////////////
//
//  emit_* procedures
//
//  emit_X  writes code for operation "X" to the output stream.
//  There is an emit_X for each opcode X, as well as emit_ functions
//  for generating names according to the naming conventions (see emit.h)
//  and calls to support functions defined in the trap handler.
//
//  Register names and addresses are passed as strings.  See `emit.h'
//  for symbolic names you can use to refer to the strings.
//
//////////////////////////////////////////////////////////////////////////////

static void emit_load(char *dest_reg, int offset, char *source_reg, ostream& s)
{
  s << LW << dest_reg << " " << offset * WORD_SIZE << "(" << source_reg << ")" 
    << endl;
}

static void emit_store(char *source_reg, int offset, char *dest_reg, ostream& s)
{
  s << SW << source_reg << " " << offset * WORD_SIZE << "(" << dest_reg << ")"
      << endl;
}

static void emit_load_imm(char *dest_reg, int val, ostream& s)
{ s << LI << dest_reg << " " << val << endl; }

static void emit_load_address(char *dest_reg, char *address, ostream& s)
{ s << LA << dest_reg << " " << address << endl; }


static void emit_addr_for_obj(char *dest_reg, Symbol sym, ostream& s)
{
  s << LA << dest_reg << " " <<sym << PROTOBJ_SUFFIX << endl;
}

static void emit_partial_load_address(char *dest_reg, ostream& s)
{ s << LA << dest_reg << " "; }

static void emit_load_bool(char *dest, const BoolConst& b, ostream& s)
{
  emit_partial_load_address(dest,s);
  b.code_ref(s);
  s << endl;
}

static void emit_load_string(char *dest, StringEntry *str, ostream& s)
{
  emit_partial_load_address(dest,s);
  str->code_ref(s);
  s << endl;
}

static void emit_load_const(char *dest_reg, StringEntry *str, ostream& s)
{
  s << LA << dest_reg << " "; str->code_ref(s); s << endl;
}

static void emit_load_int(char *dest, IntEntry *i, ostream& s)
{
  emit_partial_load_address(dest,s);
  i->code_ref(s);
  s << endl;
}

static void emit_move(char *dest_reg, char *source_reg, ostream& s)
{ s << MOVE << dest_reg << " " << source_reg << endl; }

static void emit_neg(char *dest, char *src1, ostream& s)
{ s << NEG << dest << " " << src1 << endl; }

static void emit_not(char *dest, char *src1, ostream& s) {
  s << NOT << dest << " " << src1 << endl;
}

static void emit_add(char *dest, char *src1, char *src2, ostream& s)
{ s << ADD << dest << " " << src1 << " " << src2 << endl; }

static void emit_addu(char *dest, char *src1, char *src2, ostream& s)
{ s << ADDU << dest << " " << src1 << " " << src2 << endl; }

static void emit_addiu(char *dest, char *src1, int imm, ostream& s)
{ s << ADDIU << dest << " " << src1 << " " << imm << endl; }

static void emit_div(char *dest, char *src1, char *src2, ostream& s)
{ s << DIV << dest << " " << src1 << " " << src2 << endl; }

static void emit_mul(char *dest, char *src1, char *src2, ostream& s)
{ s << MUL << dest << " " << src1 << " " << src2 << endl; }

static void emit_sub(char *dest, char *src1, char *src2, ostream& s)
{ s << SUB << dest << " " << src1 << " " << src2 << endl; }

static void emit_sll(char *dest, char *src1, int num, ostream& s)
{ s << SLL << dest << " " << src1 << " " << num << endl; }

static void emit_sle(char *dest, char *src1, char *src2, ostream& s)
{ s << SLE << dest << " " << src1 << " " << src2 << endl; }

static void emit_slt(char *dest, char *src1, char *src2, ostream& s)
{ s << SLT << dest << " " << src1 << " " << src2 << endl; }

static void emit_seq(char *dest, char *src1, char *src2, ostream& s)
{ s << SEQ << dest << " " << src1 << " " << src2 << endl; }

static void emit_jalr(char *dest, ostream& s)
{ s << JALR << "\t" << dest << endl; }

static void emit_jal_init(char *address, ostream &s) {
  s << JAL << address << CLASSINIT_SUFFIX << endl;
}

static void emit_jal(char *address,ostream &s)
{ s << JAL << address << endl; }

static void emit_return(ostream& s)
{ s << RET << endl; }

static void emit_gc_assign(ostream& s)
{ s << JAL << "_GenGC_Assign" << endl; }

static void emit_disptable_ref(Symbol sym, ostream& s)
{  s << sym << DISPTAB_SUFFIX; }

static void emit_disptable_def(Symbol sym, ostream& s) 
{
   emit_disptable_ref(sym, s); s << LABEL;
}

static void emit_init_ref(Symbol sym, ostream& s)
{ s << sym << CLASSINIT_SUFFIX; }

static void emit_init_def(Symbol sym, ostream& s) {
  emit_init_ref(sym, s);
  s << LABEL;
}
static void emit_label_ref(int l, ostream &s)
{ s << "label" << l; }

static void emit_protobj_ref(Symbol sym, ostream& s)
{ s << sym << PROTOBJ_SUFFIX; }

static void emit_protobj_def(Symbol sym, ostream& s)
{
   emit_protobj_ref(sym, s); s << LABEL;
}

static void emit_method_ref(Symbol classname, Symbol methodname, ostream& s)
{ s << classname << METHOD_SEP << methodname; }

static void emit_class_nametab_def(ostream& s)
{ s << CLASSNAMETAB << LABEL; }

static void emit_class_objtab_def(ostream& s)
{ s << CLASSOBJTAB << LABEL; }

static void emit_label_def(int l, ostream &s)
{
  emit_label_ref(l,s);
  s << ":" << endl;
}

static void emit_beqz(char *source, int label, ostream &s)
{
  s << BEQZ << source << " ";
  emit_label_ref(label,s);
  s << endl;
}

static void emit_beq(char *src1, char *src2, int label, ostream &s)
{
  s << BEQ << src1 << " " << src2 << " ";
  emit_label_ref(label,s);
  s << endl;
}

static void emit_bne(char *src1, char *src2, int label, ostream &s)
{
  s << BNE << src1 << " " << src2 << " ";
  emit_label_ref(label,s);
  s << endl;
}

static void emit_bleq(char *src1, char *src2, int label, ostream &s)
{
  s << BLEQ << src1 << " " << src2 << " ";
  emit_label_ref(label,s);
  s << endl;
}

static void emit_blt(char *src1, char *src2, int label, ostream &s)
{
  s << BLT << src1 << " " << src2 << " ";
  emit_label_ref(label,s);
  s << endl;
}

static void emit_blti(char *src1, int imm, int label, ostream &s)
{
  s << BLT << src1 << " " << imm << " ";
  emit_label_ref(label,s);
  s << endl;
}

static void emit_bgti(char *src1, int imm, int label, ostream &s)
{
  s << BGT << src1 << " " << imm << " ";
  emit_label_ref(label,s);
  s << endl;
}

static void emit_branch(int l, ostream& s)
{
  s << BRANCH;
  emit_label_ref(l,s);
  s << endl;
}

//
// Push a register on the stack. The stack grows towards smaller addresses.
//
static void emit_push(char *reg, ostream& str)
{
  emit_store(reg,0,SP,str);
  emit_addiu(SP,SP,-4,str);

}

//
// Fetch the integer value in an Int object.
// Emits code to fetch the integer value of the Integer object pointed
// to by register source into the register dest
//
static void emit_fetch_int(char *dest, char *source, ostream& s)
{ emit_load(dest, DEFAULT_OBJFIELDS, source, s); }

//
// Emits code to store the integer value contained in register source
// into the Integer object pointed to by dest.
//
static void emit_store_int(char *source, char *dest, ostream& s)
{ emit_store(source, DEFAULT_OBJFIELDS, dest, s); }


static void emit_test_collector(ostream &s)
{
  emit_push(ACC, s);
  emit_move(ACC, SP, s); // stack end
  emit_move(A1, ZERO, s); // allocate nothing
  s << JAL << gc_collect_names[cgen_Memmgr] << endl;
  emit_addiu(SP,SP,4,s);
  emit_load(ACC,0,SP,s);
}

static void emit_gc_check(char *source, ostream &s)
{
  if (source != (char*)A1) emit_move(A1, source, s);
  s << JAL << "_gc_check" << endl;
}

static void emit_div_zero(int line_number, Symbol sym, ostream& s) {
  s << sym << ":"<<line_number<<" " <<"Division by zero." <<endl;
}

// Helper function, returns largest int in vector

static int max(std::vector<int>& temporaries)
{
/*
  if (cgen_debug)
    cout << "Calculating number of temporaries needed in array: "; */

  // Make sure array has elements
  int max;
  if (temporaries.size() > 0) {
    max = temporaries[0];
  } 
  else {
    if (cgen_debug)
      cout << "\tRecieved empty array. Returning 0." << endl;
      return 0;
  }

  for (int i = 0; i < (int) temporaries.size(); i++) {
    //if (cgen_debug) cout << temporaries[i] << "; ";
    max = temporaries[i] > max ? temporaries[i] : max;
  }

/*
  if (cgen_debug)
    cout << "MAX: " << max << endl;*/
  return max;
}

static int find_method_offset(CgenClassTable* c, Symbol class_name, Symbol meth_name) 
{
  CgenNodeP caller_class = class_name == SELF_TYPE ? c->get_curr_class() : c->find_class(class_name);
  
  if (cgen_debug) cout << "\tMETH: " << meth_name << " CLASS: " 
    << caller_class->get_name();
  int offset = 0;
  for (std::vector<meth_disp>::iterator it = ((c->dispatch_tbl)[caller_class]).begin();
      it != ((c->dispatch_tbl)[caller_class]).end(); it++){ 
    if (it->meth_name == meth_name) {
      if (cgen_debug) cout << " OFFSET: " << offset << endl;     
      return offset;
    }
    offset++;
  }

  if (cgen_debug) cout << " OFFSET: " << -1 << endl;
  // Should never happen: cannot find the right method
  return -1;
}


static void emit_new_return_object(Symbol sym, ostream& s) 
{
  emit_addr_for_obj(ACC, sym, s);
  char* addr = "Object.copy";
  emit_jal(addr,s);
}


///////////////////////////////////////////////////////////////////////////////
//
// coding strings, ints, and booleans
//
// Cool has three kinds of constants: strings, ints, and booleans.
// This section defines code generation for each type.
//
// All string constants are listed in the global "stringtable" and have
// type StringEntry.  StringEntry methods are defined both for String
// constant definitions and references.
//
// All integer constants are listed in the global "inttable" and have
// type IntEntry.  IntEntry methods are defined for Int
// constant definitions and references.
//
// Since there are only two Bool values, there is no need for a table.
// The two booleans are represented by instances of the class BoolConst,
// which defines the definition and reference methods for Bools.
//
///////////////////////////////////////////////////////////////////////////////

//
// Strings
//
void StringEntry::code_ref(ostream& s)
{
  s << STRCONST_PREFIX << index;
}

//
// Emit code for a constant String.
// You should fill in the code naming the dispatch table.
//

void StringEntry::code_def(ostream& s, int stringclasstag)
{
  IntEntryP lensym = inttable.add_int(len);

  // Add -1 eye catcher
  s << WORD << "-1" << endl;

  code_ref(s);  s  << LABEL                                             // label
      << WORD << stringclasstag << endl                                 // tag
      // len: ASCII stores string constants C style. (len + 4)/2 calculates
      // the aligned size of the string constant.
      << WORD << (DEFAULT_OBJFIELDS + STRING_SLOTS + (len+4)/4) << endl // size
      << WORD << STRINGNAME << DISPTAB_SUFFIX << endl;                  // dispatch table


 /***** TODO: Add dispatch information for class String ******/

      s << WORD;  lensym->code_ref(s);  s << endl;            // string length
                                                              // as value in int_table
  emit_string_constant(s,str);                                // ascii string
  s << ALIGN;                                                 // align to word
}

//
// StrTable::code_string
// Generate a string object definition for every string constant in the 
// stringtable.
//
void StrTable::code_string_table(ostream& s, int stringclasstag)
{  
  for (List<StringEntry> *l = tbl; l; l = l->tl())
    l->hd()->code_def(s,stringclasstag);
}

//
// Ints
//
void IntEntry::code_ref(ostream &s)
{
  s << INTCONST_PREFIX << index;
}

//
// Emit code for a constant Integer.
// You should fill in the code naming the dispatch table.
//

void IntEntry::code_def(ostream &s, int intclasstag)
{
  // Add -1 eye catcher
  s << WORD << "-1" << endl;

  code_ref(s);  s << LABEL                                // label
      << WORD << intclasstag << endl                      // class tag
      << WORD << (DEFAULT_OBJFIELDS + INT_SLOTS) << endl  // object size
      << WORD << INTNAME << DISPTAB_SUFFIX << endl;      // dispatch table 

 /***** Add dispatch information for class Int ******/

      s << WORD << str << endl;                           // integer value
}


//
// IntTable::code_string_table
// Generate an Int object definition for every Int constant in the
// inttable.
//
void IntTable::code_string_table(ostream &s, int intclasstag)
{
  for (List<IntEntry> *l = tbl; l; l = l->tl())
    l->hd()->code_def(s,intclasstag);
}


//
// Bools
//
BoolConst::BoolConst(int i) : val(i) { assert(i == 0 || i == 1); }

void BoolConst::code_ref(ostream& s) const
{
  s << BOOLCONST_PREFIX << val;
}
  
//
// Emit code for a constant Bool.
// You should fill in the code naming the dispatch table.
//

void BoolConst::code_def(ostream& s, int boolclasstag)
{
  // Add -1 eye catcher
  s << WORD << "-1" << endl;

  code_ref(s);  s << LABEL                                  // label
      << WORD << boolclasstag << endl                       // class tag
      << WORD << (DEFAULT_OBJFIELDS + BOOL_SLOTS) << endl   // object size
      << WORD << BOOLNAME << DISPTAB_SUFFIX << endl;       // dispatch table

 /***** Add dispatch information for class Bool ******/

      s << WORD << val << endl;                             // value (0 or 1)
}

//////////////////////////////////////////////////////////////////////////////
//
//  CgenClassTable methods
//
//////////////////////////////////////////////////////////////////////////////

//***************************************************
//
//  Emit code to start the .data segment and to
//  declare the global names.
//
//***************************************************

void CgenClassTable::code_global_data()
{
  Symbol main    = idtable.lookup_string(MAINNAME);
  Symbol string  = idtable.lookup_string(STRINGNAME);
  Symbol integer = idtable.lookup_string(INTNAME);
  Symbol boolc   = idtable.lookup_string(BOOLNAME);

  str << "\t.data\n" << ALIGN;
  //
  // The following global names must be defined first.
  //
  str << GLOBAL << CLASSNAMETAB << endl;
  str << GLOBAL; emit_protobj_ref(main,str);    str << endl;
  str << GLOBAL; emit_protobj_ref(integer,str); str << endl;
  str << GLOBAL; emit_protobj_ref(string,str);  str << endl;
  str << GLOBAL; falsebool.code_ref(str);  str << endl;
  str << GLOBAL; truebool.code_ref(str);   str << endl;
  str << GLOBAL << INTTAG << endl;
  str << GLOBAL << BOOLTAG << endl;
  str << GLOBAL << STRINGTAG << endl;

  //
  // We also need to know the tag of the Int, String, and Bool classes
  // during code generation.
  //
  str << INTTAG << LABEL
      << WORD << intclasstag << endl;
  str << BOOLTAG << LABEL 
      << WORD << boolclasstag << endl;
  str << STRINGTAG << LABEL 
      << WORD << stringclasstag << endl;    
}


//***************************************************
//
//  Emit code to start the .text segment and to
//  declare the global names.
//
//***************************************************

void CgenClassTable::code_global_text()
{
  str << GLOBAL << HEAP_START << endl
      << HEAP_START << LABEL 
      << WORD << 0 << endl
      << "\t.text" << endl
      << GLOBAL;
  emit_init_ref(idtable.add_string("Main"), str);
  str << endl << GLOBAL;
  emit_init_ref(idtable.add_string("Int"),str);
  str << endl << GLOBAL;
  emit_init_ref(idtable.add_string("String"),str);
  str << endl << GLOBAL;
  emit_init_ref(idtable.add_string("Bool"),str);
  str << endl << GLOBAL;
  emit_method_ref(idtable.add_string("Main"), idtable.add_string("main"), str);
  str << endl;
}

void CgenClassTable::code_bools(int boolclasstag)
{
  falsebool.code_def(str,boolclasstag);
  truebool.code_def(str,boolclasstag);
}

void CgenClassTable::code_select_gc()
{
  //
  // Generate GC choice constants (pointers to GC functions)
  //
  str << GLOBAL << "_MemMgr_INITIALIZER" << endl;
  str << "_MemMgr_INITIALIZER:" << endl;
  str << WORD << gc_init_names[cgen_Memmgr] << endl;
  str << GLOBAL << "_MemMgr_COLLECTOR" << endl;
  str << "_MemMgr_COLLECTOR:" << endl;
  str << WORD << gc_collect_names[cgen_Memmgr] << endl;
  str << GLOBAL << "_MemMgr_TEST" << endl;
  str << "_MemMgr_TEST:" << endl;
  str << WORD << (cgen_Memmgr_Test == GC_TEST) << endl;
}


//********************************************************
//
// Emit code to reserve space for and initialize all of
// the constants.  Class names should have been added to
// the string table (in the supplied code, is is done
// during the construction of the inheritance graph), and
// code for emitting string constants as a side effect adds
// the string's length to the integer table.  The constants
// are emmitted by running through the stringtable and inttable
// and producing code for each entry.
//
//********************************************************

void CgenClassTable::code_constants()
{
  //
  // Add constants that are required by the code generator.
  //
  // NOTE: These are also the values expected as defaults for Int/Str
  stringtable.add_string("");
  inttable.add_string("0");

  stringtable.code_string_table(str,stringclasstag);
  inttable.code_string_table(str,intclasstag);
  code_bools(boolclasstag);
}


void CgenClassTable::code_class_nametab()
{

   emit_class_nametab_def(str);

   for (int i = 0; i < class_counter; i++){
      CgenNodeP curr = tag_to_class(i);

      if(cgen_debug){
         cout << "   For tag: " << i << " && class: " << curr->get_name() << endl;
      }

      StringEntry *curr_sym = stringtable.lookup_string(curr->get_name()->get_string());
      str << WORD;  curr_sym->code_ref(str);  str << endl; 
   }

}


void CgenClassTable::code_class_objtab() 
{

   emit_class_objtab_def(str);

   for (int i = 0; i < class_counter; i++){
      CgenNodeP curr = tag_to_class(i);

      if (cgen_debug){
         cout << "   Emitting protObj and init for class: " << curr->get_name() << endl;
      }

      str << WORD; emit_protobj_ref(curr->get_name(), str); str << endl;
      str << WORD; emit_init_ref(curr->get_name(), str);    str << endl;
   }

}

void CgenClassTable::code_dispatch_table() 
{
   build_dispatch_tbl(root());
   for (std::map<CgenNodeP, std::vector<meth_disp> >::iterator it = dispatch_tbl.begin();
      it != dispatch_tbl.end(); it++) {
      emit_disptable_def(it->first->get_name(), str);
      for (unsigned int i = 0; i < it->second.size(); i++){
         str << WORD; emit_method_ref(it->second[i].class_name,
            it->second[i].meth_name, str); str << endl;
      }
   }
}

void CgenClassTable::attr_ref(Feature curr_feature)
{
   str << WORD;
   Symbol curr_type = curr_feature->get_type();
   if (curr_type == Str) {
      StringEntry *curr_sym = stringtable.lookup_string("");
      curr_sym->code_ref(str);
   }
   else if (curr_type == Int) {
      IntEntry *curr_sym = inttable.lookup_string("0");
      curr_sym->code_ref(str);
   }
   else if (curr_type == Bool) {
      str << BOOLCONST_PREFIX << 0;
   }
   else {
      str << 0;
   }

   str << endl;
}

void CgenClassTable::code_prototype_objects()
{
   build_attr_tbl(root());
   for (std::map<CgenNodeP, std::vector<Feature> >::iterator it = attr_tbl.begin();
      it != attr_tbl.end(); it++) {
      str << WORD << "-1" << endl;                         // print -1 eye-catcher
      emit_protobj_def(it->first->get_name(), str);
      str << WORD << class_tags[it->first] << endl         // class tag
          << WORD << (DEFAULT_OBJFIELDS + (int) it->second.size()) << endl    // size
          << WORD; emit_disptable_ref(it->first->get_name(), str); str << endl;  // dispatch
      for (unsigned int i = 0; i < it->second.size(); i++) {
         attr_ref(it->second[i]);
      }
   }
}
void CgenClassTable::code_object_initializers() 
{
  curr_class = root();
  build_object_initializers(root(), 0);
}


// Iterates through every class in order and generates code
// for class methods
void CgenClassTable::code_class_methods() 
{
  // TODO:
  //  1. Recurse through every class
  //  2. For each class, iterate through methods in the class
  //  3. Code each method
  for(List<CgenNode> *l = nds; l; l = l->tl()){
    if(cgen_debug) cout << "Coding methods for class: " << l->hd()->get_name() << endl;
    curr_class = l->hd();
    code_methods(curr_class);
  }

}

// Given a class, goes through all the methods in that class's
// dispatch table. If the method was defined in the current class
// and the class is not a basic class, then it generates code for 
// that method.

void CgenClassTable::code_methods(CgenNodeP curr)
{
  if (cgen_debug) cout << "\tChecking methods" << endl;
  if (curr->basic()){
    if (cgen_debug) cout << "\tClass is a basic class, returning." << endl;
    return;     // methods of basic classes already defined
  }
  new_scope();
  add_attr(curr);

  if (cgen_debug) cout << "\t Iterating through methods" << endl;
  for (std::vector<meth_disp>::iterator it = (dispatch_tbl[curr]).begin(); 
    it != (dispatch_tbl[curr]).end(); it++) {
    
    if (curr->get_name() != it->class_name) continue;
    emit_method_ref(curr->get_name(), it->meth_name, str); str << LABEL; // emit label
    code_method(curr, it->method);
  }

  end_scope();

}

void CgenClassTable::code_method(CgenNodeP curr, Feature method)
{
  int num_temp = method->get_body()->get_nt();
  if (cgen_debug) cout << "\t" << method->get_name() << " NT: " << num_temp << endl;
  enter_method(num_temp);

  // add the formals to the environment table
  Formals formals = method->get_formals();
  // the first formal is immediately above the stack pointer
  int formal_offset = formals->len();
  for (int i = formals->first(); formals->more(i); i = formals->next(i)) {

    if (cgen_debug) cout << "\tFormal " << 
      formals->nth(i)->get_name() << " Offset: " << formal_offset << endl;

    location *new_loc = new location;
    new_loc->offset = formal_offset;
    new_loc->type = Parameter;
    add_env(formals->nth(i)->get_name(), new_loc);
    
    formal_offset--;
  }

  int ar_size = num_temp + AR_METADATA;
  emit_method_init(ar_size);

  // emit code for the body of the expression
  method->get_body()->code(str, this);

  emit_method_ret(ar_size, method->get_formals()->len());
  if (cgen_debug) cout << method->get_name() << " NT: " << num_temp << endl;

  exit_method();

}


//********************************************************
//
// These are support functions
//
//********************************************************

// This function traverses the inheritance graph (DFS) and assigns
// tags in order of visits

void CgenClassTable::assign_class_tags(CgenNodeP curr)
{
   class_tags.insert(std::pair<CgenNodeP, int>(curr, class_counter));
   if(cgen_debug){
      cout << " Class: " << curr->get_name() << " && Tag: " << class_counter << endl;
   }
   class_counter++;
   for (List<CgenNode> *l = curr->get_children(); l; l = l->tl()){

      assign_class_tags(l->hd());
   }
}

void CgenClassTable::max_class_tag(CgenNodeP curr, int& max) {
  if(max < class_tags[curr]) max = class_tags[curr];
  for (List<CgenNode> *l = curr->get_children(); l; l = l->tl()){
      max_class_tag(l->hd(), max);
   }
}

void CgenClassTable::assign_max_class_tags(CgenNodeP curr) {
  int max = 0;
  max_class_tag(curr, max);
  max_class_tags.insert(std::pair<CgenNodeP, int>(curr, max));
  if(cgen_debug){
      cout << " Class: " << curr->get_name() << " && Max class Tag: " << max << endl;
   }
  for (List<CgenNode> *l = curr->get_children(); l; l = l->tl()){
      assign_max_class_tags(l->hd());
   }
}

// This function iterates through the map to find Str, Int, and Bool
// It updates the class tags for these basic classes as necessary
void CgenClassTable::assign_basic_tags()
{
   for (std::map<CgenNodeP, int>::iterator it = class_tags.begin(); it != class_tags.end(); it++) {
      if (it->first->get_name() == Str){
        if (cgen_debug) cout << " Set String class tag to: " << it->second << endl;
        stringclasstag = it->second;
      } 
      else if (it->first->get_name() == Int){
        if (cgen_debug) cout << " Set Int class tag to: " << it->second << endl;
        intclasstag = it->second;
      }
      else if (it->first->get_name() == Bool){
        if (cgen_debug) cout << " Set Bool class tag to: " << it->second << endl;
        boolclasstag = it->second;
      }
   }
}

// Reverses the map from class tag to CgenNode
CgenNodeP CgenClassTable::tag_to_class(int tag)
{
   for (std::map<CgenNodeP, int>::iterator it = class_tags.begin(); it != class_tags.end(); it++) {
      if (it->second == tag) return it->first;
   }

   return NULL;
}

int CgenClassTable::find_redef(Symbol method_name, std::vector<meth_disp>& disp_tab) {
   for (int i = 0; i < (int) disp_tab.size(); i++) {
      if (disp_tab[i].meth_name == method_name) {
         if (cgen_debug) cout << "     Found redefinition of method " << method_name <<" at position " << i <<endl;
         return i;
      }
   }
   return -1;
}

void CgenClassTable::build_dispatch_tbl(CgenNodeP curr)
{
   if (cgen_debug) cout << "  Adding dispatch for class: " << curr->get_name() << endl;
   std::vector<meth_disp> curr_meth;
   if (curr->get_parentnd()->get_name() != No_class) {
      curr_meth = dispatch_tbl[curr->get_parentnd()];
   }
   curr_meth = build_dispatch_tbl(curr, curr_meth);
   dispatch_tbl[curr] = curr_meth;
   for (List<CgenNode> *l = curr->get_children(); l; l = l->tl()){
      build_dispatch_tbl(l->hd());
   }
}

std::vector<meth_disp> CgenClassTable::build_dispatch_tbl(CgenNodeP curr, 
   std::vector<meth_disp> curr_meth) 
{
   Features features = curr->get_features();
   if (cgen_debug) cout << "     Getting methods from class " <<curr->get_name() << endl;
   for (int i = features->first(); features->more(i); i = features->next(i)) {
      Feature feature = features->nth(i);
      if (feature->is_method()) {
         int pos = find_redef(feature->get_name(), curr_meth); 
         if (pos != -1) {
            // redefinition found 
            curr_meth[pos].class_name = curr->get_name();
            curr_meth[pos].method = feature;
         }
         else {
            if (cgen_debug) cout << "     No method " <<feature->get_name() << " found" << endl;

            meth_disp new_meth = {curr->get_name(), feature->get_name(), feature};
            curr_meth.push_back(new_meth);
         }
      }
   }
   return curr_meth;
}

void CgenClassTable::build_attr_tbl(CgenNodeP curr)
{
   if (cgen_debug) cout << "  Adding attributes for class: " << curr->get_name() << endl;
   std::vector<Feature> curr_attr;
   if (curr->get_parentnd()->get_name() != No_class) {
      curr_attr = attr_tbl[curr->get_parentnd()];
   }
   curr_attr = build_attr_tbl(curr, curr_attr);
   attr_tbl[curr] = curr_attr;
   for (List<CgenNode> *l = curr->get_children(); l; l = l->tl()){
      build_attr_tbl(l->hd());
   }
}

std::vector<Feature> CgenClassTable::build_attr_tbl(CgenNodeP curr,
   std::vector<Feature> curr_attr)
{
   Features features = curr->get_features();
   if (cgen_debug) cout << "     Getting attributes from class " <<curr->get_name() << endl;
   for (int i = features->first(); features->more(i); i = features->next(i)) {
      Feature feature = features->nth(i);
      if (!feature->is_method()) {
         if (cgen_debug) cout << "     Pushing attribute: " << feature->get_name() << endl;
         curr_attr.push_back(feature);
         }
      }
   return curr_attr; 
}

void CgenClassTable::build_object_initializers(CgenNodeP curr, int counter) 
{  
  if (cgen_debug) {  
    cout<<"Building initializer for class " << curr->get_name() 
        << " with starting offset: " << counter << endl;
  }

  std::vector<Feature> attributes = attr_tbl[curr];
  int num_temp = curr->basic() ? 0 : init_get_nt(attributes, counter);
  enter_method(num_temp);
  new_scope();
  add_attr(curr);

  int ar_size = num_temp + AR_METADATA;
  emit_init_def(curr->get_name(), str);
  emit_method_init(ar_size);

  if (curr->get_name() != Object)
    emit_jal_init(curr->get_parentnd()->get_name()->get_string(), str);

  Symbol curr_name = curr->get_name();
  if (!curr->basic()) {
    for (int i = counter; i < (int)attributes.size(); i++) {
        if(cgen_debug) {cout<<"\tIncrementing count right now"<<endl;}
        if (attributes[i]->get_initializer()->get_type() == NULL)  {
            counter++;
            continue;
          }
        if (cgen_debug) { cout<<"\tChecking attribute type of "<<attributes[i]->get_name()<<endl; }
        if (cgen_debug) { cout<<"\tInitializer Type is "<<attributes[i]->get_initializer()->get_type() << endl; }
        if (attributes[i]->get_initializer()->get_type() != No_type) {
          attributes[i]->get_initializer()->code(str, this);
          if(cgen_debug) cout<<"\t\tCounter value : "<<i<<endl;
          emit_store(ACC, DEFAULT_OBJFIELDS + i, SELF, str);
          if (cgen_Memmgr != GC_NOGC){
            emit_addiu(A1, SELF, WORD_SIZE * (DEFAULT_OBJFIELDS + i), str);
            emit_jal(GC_ASSIGN, str);
          }
        }
        counter++;
    }
  }

  emit_move(ACC, SELF, str);
  emit_method_ret(ar_size, 0);
  exit_method();
  end_scope();

  for (List<CgenNode> *l = curr->get_children(); l; l = l->tl()){
    curr_class = l->hd();
    build_object_initializers(l->hd(), counter);
  }
}

int CgenClassTable::init_get_nt(std::vector<Feature> attributes, int counter)
{

  std::vector<int> temporaries;
  temporaries.push_back(0);
  for (int i = counter; i < (int) attributes.size(); i++){
    Expression init = attributes[i]->get_initializer();
    if (init->get_type() != NULL && init->get_type() != No_type)
      temporaries.push_back(init->get_nt());
  }

  if (cgen_debug) cout << "\tATTR NT: " << max(temporaries) << endl;
  return max(temporaries);
}

// Takes in total activation record size in words and decrements
// $sp by the approprpiate number of bytes. Then saves $fp, $s0,
// and $ra
void CgenClassTable::emit_method_init(int ar_size)
{
  // first decrement sp
  emit_addiu(SP, SP, - WORD_SIZE * ar_size, str);
  // store $fp where sp used to be
  emit_store(FP, ar_size, SP, str);   
  // store $s0 immediately below $fp
  emit_store(SELF, ar_size - 1, SP, str);
  // store $ra immediately below $s0
  emit_store(RA, ar_size - 2, SP, str);
  // set $fp to old location of $sp
  emit_addiu(FP, SP, WORD_SIZE * ar_size, str);
  emit_move(SELF, ACC, str);
}


void CgenClassTable::emit_method_ret(int ar_size, int num_params)
{
  emit_load(FP, ar_size, SP, str);
  emit_load(SELF, ar_size - 1, SP, str);
  emit_load(RA, ar_size - 2, SP, str);
  emit_addiu(SP, SP, WORD_SIZE * (ar_size + num_params), str);
  emit_return(str);

}


// Helper function that finds the right CgenNodeP
// for a given class symbol.
// NOTE: Grossly inefficient
CgenNodeP CgenClassTable::find_class(Symbol find_class)
{
  // iterates through every class
  for(List<CgenNode> *l = nds; l; l = l->tl()){
    CgenNodeP curr = l->hd();
    if (curr->get_name() == find_class) return curr;
  } 
  
  // should never happen
  return NULL;
}

void CgenClassTable::add_attr(CgenNodeP curr)
{
  // Sets up the attribute offsets 
  int attr_offset = DEFAULT_OBJFIELDS;
  for (std::vector<Feature>::iterator it = (attr_tbl[curr]).begin();
    it != (attr_tbl[curr]).end(); it++) {
    location *new_loc = new location;
    new_loc->offset = attr_offset;
    new_loc->type = Attribute;
    add_env((*it)->get_name(), new_loc);
   /* if((*it)->get_initializer()->is_no_expr() == false) {
      (*it)->get_initializer()->
    }*/
    if (cgen_debug) cout << "\tATTR: " << (*it)->get_name() << " OFFSET: " 
      << attr_offset << endl;

    attr_offset++;
  }
}


//********************************************************
//
// These functions define our interface for managing the
// environment and the temporary store of methods.
//
//********************************************************

void CgenClassTable::enter_method(int number_temps) {
  new_scope();
  max_temps = number_temps;
  temp_number = 0;
  while(!temporaries.empty()) {
    temporaries.pop();
  };
}

void CgenClassTable::exit_method() {
  end_scope();
}

void CgenClassTable::enter_expr() {
  temporaries.push(temp_number);
}

void CgenClassTable::exit_expr() {
  temp_number = temporaries.top();
  temporaries.pop();
}
/* Returns the index for a new temporary in the current frame */
int CgenClassTable::get_temp() {
  int return_value = temp_number;
  assert(return_value < max_temps); // Check if not using more temporaries than allocated
  temp_number++;
  return - (3 + return_value); // first 3 past $fp hold stored values
}

location* CgenClassTable::get_env(Symbol sym) {
  return env->lookup(sym);
}

void CgenClassTable::add_env(Symbol sym, location * new_loc) {
  env->addid(sym, new_loc);
}

//********************************************************
//
// These are code emiting functions
//
//********************************************************

CgenClassTable::CgenClassTable(Classes classes, ostream& s) : nds(NULL) , str(s)
{

   stringclasstag = 0 /* Change to your String class tag here */;
   intclasstag =    0 /* Change to your Int class tag here */;
   boolclasstag =   0 /* Change to your Bool class tag here */;
   class_counter =  0;
   temp_number = 0; // initialize number of temporaries to 0
   max_temps = 0;
   label_counter = 0;
   env = new SymbolTable<Symbol, location>;
   enterscope();
   if (cgen_debug) cout << "Building CgenClassTable" << endl;
   install_basic_classes();
   install_classes(classes);
   build_inheritance_tree();

   if (cgen_debug) cout << "Assigning class tags" << endl;
   assign_class_tags(root());
   if (cgen_debug) cout << "Updating class tags for Str, Int, Bool" << endl;
   assign_basic_tags();
   assign_max_class_tags(root());
   code();
   exitscope();
}

void CgenClassTable::install_basic_classes()
{

// The tree package uses these globals to annotate the classes built below.
  //curr_lineno  = 0;
  Symbol filename = stringtable.add_string("<basic class>");

//
// A few special class names are installed in the lookup table but not
// the class list.  Thus, these classes exist, but are not part of the
// inheritance hierarchy.
// No_class serves as the parent of Object and the other special classes.
// SELF_TYPE is the self class; it cannot be redefined or inherited.
// prim_slot is a class known to the code generator.
//
  addid(No_class,
	new CgenNode(class_(No_class,No_class,nil_Features(),filename),
			    Basic,this));
  addid(SELF_TYPE,
	new CgenNode(class_(SELF_TYPE,No_class,nil_Features(),filename),
			    Basic,this));
  addid(prim_slot,
	new CgenNode(class_(prim_slot,No_class,nil_Features(),filename),
			    Basic,this));

// 
// The Object class has no parent class. Its methods are
//        cool_abort() : Object    aborts the program
//        type_name() : Str        returns a string representation of class name
//        copy() : SELF_TYPE       returns a copy of the object
//
// There is no need for method bodies in the basic classes---these
// are already built in to the runtime system.
//
  install_class(
   new CgenNode(
    class_(Object, 
	   No_class,
	   append_Features(
           append_Features(
           single_Features(method(cool_abort, nil_Formals(), Object, no_expr())),
           single_Features(method(type_name, nil_Formals(), Str, no_expr()))),
           single_Features(method(copy, nil_Formals(), SELF_TYPE, no_expr()))),
	   filename),
    Basic,this));

// 
// The IO class inherits from Object. Its methods are
//        out_string(Str) : SELF_TYPE          writes a string to the output
//        out_int(Int) : SELF_TYPE               "    an int    "  "     "
//        in_string() : Str                    reads a string from the input
//        in_int() : Int                         "   an int     "  "     "
//
   install_class(
    new CgenNode(
     class_(IO, 
            Object,
            append_Features(
            append_Features(
            append_Features(
            single_Features(method(out_string, single_Formals(formal(arg, Str)),
                        SELF_TYPE, no_expr())),
            single_Features(method(out_int, single_Formals(formal(arg, Int)),
                        SELF_TYPE, no_expr()))),
            single_Features(method(in_string, nil_Formals(), Str, no_expr()))),
            single_Features(method(in_int, nil_Formals(), Int, no_expr()))),
	   filename),	    
    Basic,this));

//
// The Int class has no methods and only a single attribute, the
// "val" for the integer. 
//
   install_class(
    new CgenNode(
     class_(Int, 
	    Object,
            single_Features(attr(val, prim_slot, no_expr())),
	    filename),
     Basic,this));

//
// Bool also has only the "val" slot.
//
    install_class(
     new CgenNode(
      class_(Bool, Object, single_Features(attr(val, prim_slot, no_expr())),filename),
      Basic,this));

//
// The class Str has a number of slots and operations:
//       val                                  ???
//       str_field                            the string itself
//       length() : Int                       length of the string
//       concat(arg: Str) : Str               string concatenation
//       substr(arg: Int, arg2: Int): Str     substring
//       
   install_class(
    new CgenNode(
      class_(Str, 
	     Object,
             append_Features(
             append_Features(
             append_Features(
             append_Features(
             single_Features(attr(val, Int, no_expr())),
            single_Features(attr(str_field, prim_slot, no_expr()))),
            single_Features(method(length, nil_Formals(), Int, no_expr()))),
            single_Features(method(concat, 
				   single_Formals(formal(arg, Str)),
				   Str, 
				   no_expr()))),
	    single_Features(method(substr, 
				   append_Formals(single_Formals(formal(arg, Int)), 
						  single_Formals(formal(arg2, Int))),
				   Str, 
				   no_expr()))),
	     filename),
        Basic,this));

}

// CgenClassTable::install_class
// CgenClassTable::install_classes
//
// install_classes enters a list of classes in the symbol table.
//
void CgenClassTable::install_class(CgenNodeP nd)
{
  Symbol name = nd->get_name();

  if (probe(name))
    {
      return;
    }

  // The class name is legal, so add it to the list of classes
  // and the symbol table.
  nds = new List<CgenNode>(nd,nds);
  addid(name,nd);
}

void CgenClassTable::install_classes(Classes cs)
{
  for(int i = cs->first(); cs->more(i); i = cs->next(i))
    install_class(new CgenNode(cs->nth(i),NotBasic,this));
}

//
// CgenClassTable::build_inheritance_tree
//
void CgenClassTable::build_inheritance_tree()
{
  for(List<CgenNode> *l = nds; l; l = l->tl())
      set_relations(l->hd());
}

//
// CgenClassTable::set_relations
//
// Takes a CgenNode and locates its, and its parent's, inheritance nodes
// via the class table.  Parent and child pointers are added as appropriate.
//
void CgenClassTable::set_relations(CgenNodeP nd)
{
  CgenNode *parent_node = probe(nd->get_parent());
  nd->set_parentnd(parent_node);
  parent_node->add_child(nd);
}

void CgenNode::add_child(CgenNodeP n)
{
  children = new List<CgenNode>(n,children);
}

void CgenNode::set_parentnd(CgenNodeP p)
{
  assert(parentnd == NULL);
  assert(p != NULL);
  parentnd = p;
}



void CgenClassTable::code()
{
  if (cgen_debug) cout << "coding global data" << endl;
  code_global_data();

  if (cgen_debug) cout << "choosing gc" << endl;
  code_select_gc();

  if (cgen_debug) cout << "coding constants" << endl;
  code_constants();

//                   - class_nameTab
  if (cgen_debug) cout << "coding class name table" << endl;
  code_class_nametab();

//                   - class_objTab
  if (cgen_debug) cout << "coding class object table" << endl;
  code_class_objtab();

//                    - dispatch tables
  if (cgen_debug) cout << "coding dispatch tables" << endl;
  code_dispatch_table();
//                    - prototype objects
  if (cgen_debug) cout << "coding prototype objects" << endl;
  code_prototype_objects();
//                     - global text
  if (cgen_debug) cout << "coding global text" << endl;
  code_global_text();
//                   - object initializer
  if (cgen_debug) cout << "coding object initializers" << endl;
  code_object_initializers();

//                   - the class methods
  if (cgen_debug) cout << "coding class methods" << endl;
  code_class_methods();

  
}


CgenNodeP CgenClassTable::root()
{
   return probe(Object);
}


///////////////////////////////////////////////////////////////////////
//
// CgenNode methods
//
///////////////////////////////////////////////////////////////////////

CgenNode::CgenNode(Class_ nd, Basicness bstatus, CgenClassTableP ct) :
   class__class((const class__class &) *nd),
   parentnd(NULL),
   children(NULL),
   basic_status(bstatus)
{ 
   stringtable.add_string(name->get_string());          // Add class name to string table
}


//******************************************************************
//
//   Fill in the following methods to produce code for the
//   appropriate expression.  You may add or remove parameters
//   as you wish, but if you do, remember to change the parameters
//   of the declarations in `cool-tree.h'  Sample code for
//   constant integers, strings, and booleans are provided.
//
//*****************************************************************

void assign_class::code(ostream &s, CgenClassTable *c) {
  if (cgen_debug){ cout << "\tCGEN assign" << endl; }
  c->enter_expr();
  expr->code(s, c);
  location* curr_loc = c->get_env(name);
  if(curr_loc->type == Attribute) {
    emit_store(ACC, curr_loc->offset, SELF, s);

    // If memory manager is activated, let the GC know
    if (cgen_Memmgr != GC_NOGC){
      emit_addiu(A1, SELF, curr_loc->offset * WORD_SIZE, s);
      emit_jal(GC_ASSIGN, s);
    }
  }
  else {
    emit_store(ACC, curr_loc->offset, FP, s);
  }
  c->exit_expr();
}


/* 
  Dispatch:
  1. Evaluate arguments in order. 
  2. Push them in reverse order onto the stack
  3. Check to see if they're trying to dispatch on void
  4. Call error if dispatch is on void
  5. Dispatch
*/
void static_dispatch_class::code(ostream &s, CgenClassTable *c) {
  if (cgen_debug){ cout << "\tCGEN Static dispatch" << endl; }
  c->enter_expr();

  // TODO: Make sure stack pointer is restored properly by the end
  for (int i = actual->first(); actual->more(i); i = actual->next(i)) {
    Expression param_expr = actual->nth(i);
    param_expr->code(s, c);
    // push value onto the stack in order
    emit_push(ACC, s);
  }

  // Determine what we need to dispatch on
  expr->code(s, c);

  // Dispatch on Void check
  int new_label = c->get_label_counter();
  emit_bne(ACC, ZERO, new_label, s);

  // Set up _dispatch_abort call
  // $t1 = line number
  // $a0 = filename
  emit_load_const(ACC, stringtable.lookup_string(c->get_filename()), s);
  emit_load_imm(T1, get_line_number(), s);
  emit_jal(DISP_ABORT, s);

  // Calling dispatch
  emit_label_def(new_label, s);

  // Load the dispatch table for the parent
  emit_partial_load_address(T1, s); emit_disptable_ref(type_name, s); s << endl;

  // Determine method offset
  int offset = find_method_offset(c, type_name, name);
  emit_load(T1, offset, T1, s);

  // Call the method
  emit_jalr(T1, s);

  c->exit_expr();
}

void dispatch_class::code(ostream &s, CgenClassTable *c) {
  if (cgen_debug){ cout << "\tCGEN Dynamic dispatch" << endl; }
  c->enter_expr();

  for (int i = actual->first(); actual->more(i); i = actual->next(i)) {
    Expression param_expr = actual->nth(i);
    param_expr->code(s, c);
    // push value onto the stack in order
    emit_push(ACC, s);
  }

  expr->code(s, c);
 
  // Dispatch on Void check
  int new_label = c->get_label_counter();
  emit_bne(ACC, ZERO, new_label, s);
  
  // Set up _dispatch_abort call
  // $t1 = line number
  // $a0 = filename
  emit_load_const(ACC, stringtable.lookup_string(c->get_filename()), s);
  emit_load_imm(T1, get_line_number(), s);
  emit_jal(DISP_ABORT, s);

  // Success branch
  emit_label_def(new_label, s);
  emit_load(T1, DISPTABLE_OFFSET, ACC, s); // access dispatch table of evaluated class
  
  int offset = find_method_offset(c, expr->get_type(), name);
  emit_load(T1, offset, T1, s);
 
  emit_jalr(T1, s);

  c->exit_expr();
}

void cond_class::code(ostream &s, CgenClassTable *c) {
  if (cgen_debug){ cout << "\tCGEN Cond" << endl; }
  c->enter_expr();
  pred->code(s, c);
  emit_load(ACC, 3, ACC, s);
  int false_label_counter = c->get_label_counter();
  emit_beq(ACC, ZERO, false_label_counter, s);
  then_exp->code(s, c);
  int final_label_counter = c->get_label_counter();
  emit_branch(final_label_counter, s);
  emit_label_def(false_label_counter, s);
  else_exp->code(s, c);
  emit_label_def(final_label_counter, s);
  c->exit_expr();
}

void loop_class::code(ostream &s, CgenClassTable *c) {
  if (cgen_debug){ cout << "\tCGEN Loop" << endl; }
  c->enter_expr();
  int loop_label_counter = c->get_label_counter();
  emit_label_def(loop_label_counter, s);
  pred->code(s, c);
  emit_load(ACC, 3, ACC, s);
  int false_label_counter = c->get_label_counter();
  emit_beq(ACC, ZERO, false_label_counter, s);
  body->code(s, c);
  emit_branch(loop_label_counter, s);
  emit_label_def(false_label_counter, s);
  emit_move(ACC, ZERO, s);
  c->exit_expr();
}

void typcase_class::code(ostream &s, CgenClassTable *c) {
  if (cgen_debug){ cout << "\tCGEN Case" << endl; }
  c->new_scope();
  c->enter_expr();
  // Evaluate the case expression
  expr->code(s, c);
  // Get the label for no match
  int final_counter = c->get_label_counter();
  //Get first label
  int label_one_counter = c->get_label_counter();
  //If expression did not return void, jump to first label
  emit_bne(ACC, ZERO, label_one_counter, s);
  // else, abort!
  emit_load_const(ACC, stringtable.lookup_string(c->get_filename()), s);
  emit_load_imm(T1, get_line_number(), s);
  emit_jal(CASE2_ABORT, s);
  //all branches
  std::vector<Case> order;
  //push all branches
  for(int i = cases->first(); cases->more(i); i = cases->next(i)) {
    order.push_back(cases->nth(i));
  }
  // Sort all branches by class tag in descending order 
   int flag = 1;    // set flag to 1 to start first pass
   Case temp;             // holding variable
   int numLength = order.size( ); 
   for(int i = 1; (i <= numLength) && flag; i++) {
    flag = 0;
    for (int j=0; j < (numLength -1); j++) {
      if (c->class_tags[c->find_class(order[j+1]->get_type())] > c->class_tags[c->find_class(order[j]->get_type())])      // ascending order simply changes to <
        { 
          temp = order[j];             // swap elements
          order[j] = order[j+1];
          order[j+1] = temp;
          flag = 1;               // indicates that a swap occurred.
        }
    }
  }
     // Go through all branches in descending order 
    for(int i = 0; i < (int)order.size(); i++) {
      // Current label definition 
      emit_label_def(label_one_counter, s);
      label_one_counter = c->get_label_counter(); // next label
      // Load tag of expression evaluated
      emit_load(T1,0, ACC, s);
      // if tag is less than curren tag, jump to next label
      emit_blti(T1, c->class_tags[c->find_class(order[i]->get_type())], label_one_counter, s);
     // if tag is greater than the max current tag, jump to next label
     emit_bgti(T1, c->max_class_tags[c->find_class(order[i]->get_type())], label_one_counter, s);
      // else, add the identifier 
      location* new_loc = new location;
      new_loc->type = Local;
      int loc_offset = c->get_temp();
      new_loc->offset = loc_offset;
        // store the value of the expression
      emit_store(ACC, loc_offset, FP, s);
      c->add_env(order[i]->get_name(), new_loc);
      // get the expression of the current branch
      order[i]->get_exp()->code(s, c);
      emit_branch(final_counter, s);
    }
    emit_label_def(label_one_counter, s); // if no correct branch found, error thrown
    emit_jal(CASE_ABORT, s); // abort 
    emit_label_def(final_counter, s); // empty final counter

  c->exit_expr();
  c->end_scope();
}

void block_class::code(ostream &s, CgenClassTable *c) {
  c->enter_expr();

  for (int i = body->first(); body->more(i); i = body->next(i)) {
    Expression expr = body->nth(i);
    expr->code(s, c);
  }

  c->exit_expr();
}

void let_class::code(ostream &s, CgenClassTable *c) {
  c->enter_expr();
  c->new_scope();

  if (cgen_debug){ cout << "\tCGEN Let" << endl; }

  // initialize the new object first
  if (type_decl == Int) 
    emit_load_int(ACC,inttable.lookup_string("0"),s);
  else if (type_decl == Str) 
    emit_load_string(ACC, stringtable.lookup_string(""), s);
  else if (type_decl == Bool) 
    emit_load_bool(ACC, falsebool, s);
  else 
    emit_move(ACC, ZERO, s);

  if (cgen_debug) {
    const char* init_type = init->is_no_expr() ? "true" : "false";
    cout << "\tINIT NO_EXPR?: " << init_type << endl;
  }

  init->code(s, c);

  int loc_offset = c->get_temp();
  // store the value of the expression
  emit_store(ACC, loc_offset, FP, s);
  // add ID to the environment
  location* new_loc = new location;
  new_loc->type = Local;
  new_loc->offset = loc_offset;
  c->add_env(identifier, new_loc);

  body->code(s, c);

  c->end_scope();
  c->exit_expr();
}

void plus_class::code(ostream &s, CgenClassTable *c) {
  if (cgen_debug){ cout << "\tCGEN Plus" << endl; }
  c->enter_expr();
  e1->code(s, c);
  int temporary_index = c->get_temp();
  emit_store(ACC, temporary_index, FP, s);
  e2->code(s, c);
  emit_load(T1, temporary_index, FP, s);
  emit_load(T1, 3, T1, s);
  emit_load(ACC, 3, ACC, s);
  emit_add(ACC, T1, ACC, s);
  emit_store(ACC, temporary_index, FP, s);
  emit_new_return_object(Int, s);
  emit_load(T1, temporary_index, FP, s);
  emit_store(T1, 3, ACC, s);
  c->exit_expr();
}

void sub_class::code(ostream &s, CgenClassTable *c) {
  if (cgen_debug){ cout << "\tCGEN Sub" << endl; }
  c->enter_expr();
  e1->code(s, c);
  int temporary_index = c->get_temp();
  emit_store(ACC, temporary_index, FP, s);
  e2->code(s, c);
  emit_load(T1, temporary_index, FP, s);
  emit_load(T1, 3, T1, s);
  emit_load(ACC, 3, ACC, s);
  emit_sub(ACC, T1, ACC, s);
  emit_store(ACC, temporary_index, FP, s);
  emit_new_return_object(Int, s);
  emit_load(T1, temporary_index, FP, s);
  emit_store(T1, 3, ACC, s);
  c->exit_expr();
}

void mul_class::code(ostream &s, CgenClassTable *c) {
  if (cgen_debug){ cout << "\tCGEN Mul" << endl; }
  c->enter_expr();
  e1->code(s, c);
  int temporary_index = c->get_temp();
  emit_store(ACC, temporary_index, FP, s);
  e2->code(s, c);
  emit_load(T1, temporary_index, FP, s);
  emit_load(T1, 3, T1, s);
  emit_load(ACC, 3, ACC, s);
  emit_mul(ACC, T1, ACC, s);
  emit_store(ACC, temporary_index, FP, s);
  emit_new_return_object(Int, s);
  emit_load(T1, temporary_index, FP, s);
  emit_store(T1, 3, ACC, s);
  c->exit_expr();
}

void divide_class::code(ostream &s, CgenClassTable *c) {
  if (cgen_debug){ cout << "\tCGEN Div" << endl; }
  c->enter_expr();
  e1->code(s, c);
  int temporary_index = c->get_temp();
  emit_store(ACC, temporary_index, FP, s);
  e2->code(s, c);
  emit_load(T1, temporary_index, FP, s);
  emit_load(T1, 3, T1, s);
  emit_load(ACC, 3, ACC, s);
  emit_div(ACC, T1, ACC, s);
  emit_store(ACC, temporary_index, FP, s);
  emit_new_return_object(Int, s);
  emit_load(T1, temporary_index, FP, s);
  emit_store(T1, 3, ACC, s);
  c->exit_expr();
}

void neg_class::code(ostream &s, CgenClassTable *c) {
  if (cgen_debug){ cout << "\tCGEN Neg" << endl; }
  c->enter_expr();
  e1->code(s,c);
  emit_load(ACC, 3, ACC, s);
  emit_neg(ACC, ACC, s);
  int temporary_index = c->get_temp();
  emit_store(ACC, temporary_index, FP, s);
  emit_new_return_object(Int, s);
  emit_load(T1, temporary_index, FP, s);
  emit_store(T1, 3, ACC, s);
  c->exit_expr();
}

void lt_class::code(ostream &s, CgenClassTable *c) {
  if (cgen_debug){ cout << "\tCGEN Lt" << endl; }
  c->enter_expr();
  e1->code(s, c);
  int temporary_index = c->get_temp();
  emit_store(ACC, temporary_index, FP, s);
  e2->code(s, c);
  emit_load(T1, temporary_index, FP, s);
  emit_load(T1, 3, T1, s);
  emit_load(ACC, 3, ACC, s);
  emit_slt(ACC, T1, ACC, s);
  emit_store(ACC, temporary_index, FP, s);
  emit_new_return_object(Bool, s);
  emit_load(T1, temporary_index, FP, s);
  emit_store(T1, 3, ACC, s);
  c->exit_expr();
}

// 1. If the pointers are equal, jump to done anyways
// 2. Otherwise, set up boolconst0 and boolconst1
// 3. call equality_test

void eq_class::code(ostream &s, CgenClassTable *c) {
  if (cgen_debug){ cout << "\tCGEN eq" << endl; }
  c->enter_expr();
  e1->code(s, c);
  int temporary_index = c->get_temp();
  emit_store(ACC, temporary_index, FP, s);
  e2->code(s, c);
  emit_load(T1, temporary_index, FP, s);
  emit_move(T2, ACC, s);

  // At this point, e1 -> T1 and e2 -> T2
  // We will jump to this label when done
  int complete = c->get_label_counter();

  // equality_test expects true in $a0
  emit_load_bool(ACC, truebool, s);
  // if pointers equal, values must be equal
  emit_beq(T1, T2, complete, s);
  
  // otherwise, we must set up false in $a1
  // and let equality_test work (will handle the
  // cases with non-basic args as well)
  emit_load_bool(A1, falsebool, s);
  emit_jal(EQUALITY_TEST, s);

  emit_label_def(complete, s);

  c->exit_expr();

}

void leq_class::code(ostream &s, CgenClassTable *c) {
  if (cgen_debug){ cout << "\tCGEN Leq" << endl; }
  c->enter_expr();
  e1->code(s, c);
  int temporary_index = c->get_temp();
  emit_store(ACC, temporary_index, FP, s);
  e2->code(s, c);
  emit_load(T1, temporary_index, FP, s);
  emit_load(T1, 3, T1, s);
  emit_load(ACC, 3, ACC, s);
  emit_sle(ACC, T1, ACC, s);
  emit_store(ACC, temporary_index, FP, s);
  emit_new_return_object(Bool, s);
  emit_load(T1, temporary_index, FP, s);
  emit_store(T1, 3, ACC, s);
  c->exit_expr();
}

void comp_class::code(ostream &s, CgenClassTable *c) {

  if (cgen_debug){ cout << "\tCGEN comp" << endl; }
  c->enter_expr();
  e1->code(s,c);
  emit_load(ACC, 3, ACC, s);
  int true_label_counter = c->get_label_counter();
  int final_label_counter = c->get_label_counter();
  int temporary_index = c->get_temp();
  emit_beq(ACC, ZERO, true_label_counter, s);
  emit_move(ACC, ZERO, s);
  emit_store(ACC, temporary_index, FP, s);
  emit_new_return_object(Bool, s);
  emit_load(T1, temporary_index, FP, s);
  emit_store(T1, 3, ACC, s);
  emit_branch(final_label_counter, s);
  emit_label_def(true_label_counter, s);
  emit_load_imm(ACC, 1, s);
  emit_store(ACC, temporary_index, FP, s);
  emit_new_return_object(Bool, s);
  emit_load(T1, temporary_index, FP, s);
  emit_store(T1, 3, ACC, s);
  emit_label_def(final_label_counter, s);
  c->exit_expr();
}

void int_const_class::code(ostream& s, CgenClassTable *c)  
{
  if (cgen_debug){ cout << "\tCGEN int const" << endl; }
  //
  // Need to be sure we have an IntEntry *, not an arbitrary Symbol
  //
  c->enter_expr();
  emit_load_int(ACC,inttable.lookup_string(token->get_string()),s);
  c->exit_expr();
}

void string_const_class::code(ostream& s, CgenClassTable *c)
{
  if (cgen_debug){ cout << "\tCGEN str const" << endl; }
  c->enter_expr();
  emit_load_string(ACC,stringtable.lookup_string(token->get_string()),s);
  c->exit_expr();
}

void bool_const_class::code(ostream& s, CgenClassTable *c)
{
  if (cgen_debug){ cout << "\tCGEN bool const" << endl; }
  c->enter_expr();
  emit_load_bool(ACC, BoolConst(val), s);
  c->exit_expr();
}

void new__class::code(ostream &s, CgenClassTable *c) {
  if (cgen_debug){ cout << "\tCGEN new" << endl; }
  c->enter_expr();
  if(type_name == SELF_TYPE) {
    // 1. Find the tag of the current class
    // 2. Calculate the appropriate offset to prototype object in objTab
    // 3. Load the value at that location
    // 4. Call Object.copy
    // 5. The next location should be the init method - call that

    emit_load(T1, TAG_OFFSET, SELF, s);

    emit_load_imm(T2, 8, s); // position is tag_num * 8 bytes above objTab base
    emit_mul(T1, T1, T2, s);

    emit_load_address(T2, CLASSOBJTAB, s);
    emit_addu(T2, T2, T1, s);
    emit_load(ACC, 0, T2, s); 

    emit_jal("Object.copy", s);
    emit_load(T2, 1, T2, s);
    
    emit_jalr(T2, s);
  }
  else {
    emit_new_return_object(type_name, s);
    emit_jal_init(type_name->get_string(), s);
  }
  c->exit_expr();
}

void isvoid_class::code(ostream &s, CgenClassTable *c) {
  if (cgen_debug){ cout << "\tCGEN isvoid" << endl; }
  c->enter_expr();
  e1->code(s, c);
  int true_label_counter = c->get_label_counter();
  int final_label_counter = c->get_label_counter();
  emit_beq(ACC, ZERO, true_label_counter, s);
  emit_new_return_object(Bool, s);
  emit_move(T1, ZERO, s);
  emit_store(T1, 3, ACC, s);
  emit_branch(final_label_counter, s);
  emit_label_def(true_label_counter, s);
  emit_new_return_object(Bool, s);
  emit_load_imm(T1, 1, s); 
  emit_store(T1, 3, ACC, s);
  emit_label_def(final_label_counter, s);
  c->exit_expr();
}

void no_expr_class::code(ostream &s, CgenClassTable *c) {
  c->enter_expr();
  if (cgen_debug) cout << "\tCGEN No_expr" << endl;
  c->exit_expr();
}

void object_class::code(ostream &s, CgenClassTable *c) {
  c->enter_expr();


  if (cgen_debug) cout << "\tCGEN OBJ_CLASS" << endl;
  location* obj_loc = c->get_env(name);

  if (name == self) {
    if (cgen_debug) cout << "\tCGEN OBJ_CLASS: self:" << endl;
    emit_move(ACC, SELF, s);
  }
  else {
    if (obj_loc->type == Attribute) {
      if (cgen_debug) cout << "\tID: " << name << "Location: $s0" << endl;
      emit_load(ACC, obj_loc->offset, SELF, s);
    }
    else {
      emit_load(ACC, obj_loc->offset, FP, s);
    }
  }

  c->exit_expr();
}



int assign_class::get_nt() 
{
  return expr->get_nt();
}

int static_dispatch_class::get_nt() 
{
  std::vector<int> temporaries;
  int expr_temp = 1; // need one slot to store the expression on which to dispatch

  for (int i = actual->first(); actual->more(i); i = actual->next(i)) {
    temporaries.push_back(expr_temp + actual->nth(i)->get_nt());
    expr_temp++;
  }

  if (cgen_debug) {
    cout << "STATIC DISPATCH NT: " << max(temporaries) << endl;
  }

  return max(temporaries);
}

int dispatch_class::get_nt() 
{
  std::vector<int> temporaries;
  int expr_temp = 1;    // need one slot to store the expression on which to dispatch

  for (int i = actual->first(); actual->more(i); i = actual->next(i)) {
    temporaries.push_back(expr_temp + actual->nth(i)->get_nt());
    expr_temp++;
  }

  if (cgen_debug) {
    cout << "DYNAMIC DISPATCH NT: " << max(temporaries) << endl;
  }

  return max(temporaries);
}

int cond_class::get_nt() 
{
  std::vector<int> temporaries;

  // each is evaluated separately, do not need to store return values
  temporaries.push_back(pred->get_nt());
  temporaries.push_back(then_exp->get_nt());
  temporaries.push_back(else_exp->get_nt());

  if (cgen_debug) {
    cout << "COND NT: " << max(temporaries) << endl;
  }

  return max(temporaries);
}

int loop_class::get_nt() 
{
  std::vector<int> temporaries;

  temporaries.push_back(pred->get_nt());
  temporaries.push_back(body->get_nt());

  if (cgen_debug)
    cout << "LOOP NT: " << max(temporaries) << endl;

  return max(temporaries);
}

// SHUBHAM: Double check that my calculations for temporaries in case
// were actually correct. We will need one to hold the return tag of
// the case expression. I think we will have one extra slot, but that
// shouldn't be a problem.
int typcase_class::get_nt() 
{

  std::vector<int> temporaries;

  temporaries.push_back(expr->get_nt()); // one temporary at least needed to hold type
  for (int i = cases->first(); cases->more(i); i = cases->next(i))
    temporaries.push_back(cases->nth(i)->get_nt()); // one temporary used to hold initialization

  if (cgen_debug)
    cout << "CASE NT: " << max(temporaries) << endl;


  return max(temporaries) + cases->len();
}

int branch_class::get_nt()
{

  int branch_temp = expr->get_nt();

  if (cgen_debug)
    cout << "BRANCH NT: " << branch_temp << endl;

  return branch_temp;
}

int block_class::get_nt() 
{

  std::vector<int> temporaries;

  for (int i = body->first(); body->more(i); i = body->next(i))
    temporaries.push_back(body->nth(i)->get_nt()); // no persistent temporaries needed

  if (cgen_debug) 
    cout << "BLOCK NT: " << max(temporaries) << endl;

  return max(temporaries);
}

int let_class::get_nt() 
{

  std::vector<int> temporaries;

  temporaries.push_back(init->get_nt());
  temporaries.push_back(body->get_nt() + 1); // one extra needed to hold init

  if (cgen_debug) 
    cout << "LET NT: " << max(temporaries) << endl;
  return max(temporaries);
}

int plus_class::get_nt() 
{
  std::vector<int> temporaries;

  temporaries.push_back(e1->get_nt());
  temporaries.push_back(e2->get_nt() + 1); // need to store value of e1 in meantime

  if (cgen_debug) 
    cout << "PLUS NT: " << max(temporaries);
  return max(temporaries);
}

int sub_class::get_nt() 
{
  std::vector<int> temporaries;

  temporaries.push_back(e1->get_nt());
  temporaries.push_back(e2->get_nt() + 1); // need to store value of e1 in meantime

  if (cgen_debug) 
    cout << "SUB NT: " << max(temporaries);
  return max(temporaries);
}

int mul_class::get_nt() 
{
  std::vector<int> temporaries;

  temporaries.push_back(e1->get_nt());
  temporaries.push_back(e2->get_nt() + 1); // need to store value of e1 in meantime

  if (cgen_debug) 
    cout << "MULT NT: " << max(temporaries);
  return max(temporaries);
}

int divide_class::get_nt() 
{
  std::vector<int> temporaries;

  temporaries.push_back(e1->get_nt());
  temporaries.push_back(e2->get_nt() + 1); // need to store value of e1 in meantime

  if (cgen_debug) 
    cout << "DIVIDE NT: " << max(temporaries);
  return max(temporaries);
}

int neg_class::get_nt() 
{
  return e1->get_nt() + 1;
}

int lt_class::get_nt() 
{
  std::vector<int> temporaries;

  temporaries.push_back(e1->get_nt());
  temporaries.push_back(1 + e2->get_nt()); // need to store return of e1 in meantime

  if (cgen_debug) 
    cout << "LT NT: " << max(temporaries) << endl;
  return max(temporaries);
}

int eq_class::get_nt() 
{
  std::vector<int> temporaries;
  // Store value of expression in
  temporaries.push_back(e1->get_nt());
  temporaries.push_back(1 + e2->get_nt()); // need to store return of e1 in meantime

  if (cgen_debug) 
    cout << "EQ NT: " << max(temporaries) << endl;
  return max(temporaries);
}

int leq_class::get_nt() 
{
  std::vector<int> temporaries;

  temporaries.push_back(e1->get_nt());
  temporaries.push_back(1 + e2->get_nt()); // need to store return of e1 in meantime

  if (cgen_debug) 
    cout << "LEQ NT: " << max(temporaries) << endl;
  return max(temporaries);
}

int comp_class::get_nt() 
{
  return e1->get_nt() + 1;

}

int int_const_class::get_nt()  
{
  return 0;
}

int string_const_class::get_nt()
{
  return 0;
}

int bool_const_class::get_nt()
{
  return 0;
}

int new__class::get_nt() 
{
  return 0;
}

int isvoid_class::get_nt() 
{
  return e1->get_nt();
}

int no_expr_class::get_nt() 
{
  return 0;
}

int object_class::get_nt() 
{
  return 0;
}

