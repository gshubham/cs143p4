(*  Example cool program testing as many aspects of the code generator
    as possible.
 *)

class Main {
	test_strconst : String <- "Test string constant";
	test_intconst : Int <- 5;
  main():Int { 0 };
};

class Test1 {
	attr1 : Int <- 1;
	attr2 : String <- "Class: Test1, attr2";
	attr3 : Object;
};

